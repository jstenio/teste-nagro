import { TesteNagroPage } from './app.po';

describe('teste-nagro App', function() {
  let page: TesteNagroPage;

  beforeEach(() => {
    page = new TesteNagroPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
