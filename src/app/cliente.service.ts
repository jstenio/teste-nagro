import {EventEmitter, Inject, Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {Cliente} from "./cliente";
import {MaterializeAction} from "angular2-materialize";

@Injectable()
export class ClienteService {

  private readonly BASE_URL: string = 'URL REQUISICAO';
  emitirClienteEditar = new EventEmitter<Cliente>();

  constructor(private http: Http) { }

  getClientes(){
    return this.http
              .get(this.BASE_URL)
              .map(res=>res.json());
  }

  createCliente(client: Cliente){
    return this.http
                .post(this.BASE_URL, client)
                .map(res=>res.json());
  }

  deletCliente(id: number){
    return this.http
                .delete(this.BASE_URL + '/' + id)
                .map(res=>res.json());
  }

}
