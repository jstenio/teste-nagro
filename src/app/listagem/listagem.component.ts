import {Component, EventEmitter, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator} from "@angular/material";
import { toast, MaterializeAction } from 'angular2-materialize';
import {Cliente} from "../cliente";
import {ClienteService} from "../cliente.service";

declare var Materialize:any;


@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.scss']
})
export class ListagemComponent {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  tooltipActions = new EventEmitter<string|MaterializeAction>();

  displayedColumns: Array<string> = ['nome', 'email', 'dataNascimento', 'sexo', 'opcoes'];
  clientes: Array<Cliente>;
  dataSource: MatTableDataSource<Cliente>;
  filtroNome: string = '';


  constructor(private clienteService: ClienteService) {
    let c1 = Cliente.clienteFromData('Cliente 1', 'cliente@email.net', '12-12-2001', 'M');
    let c2 = Cliente.clienteFromData('Cliente 2', 'cliente@teste.net', '12-12-2002', 'F');
    let c3 = Cliente.clienteFromData('Cliente 3', 'email@cliente.teste', '12-12-2003', 'F');
    this.clientes = [c1, c2, c3];
    this.dataSource  = new MatTableDataSource<Cliente>(this.clientes);
  }

  ngAfterViewInit() {

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


  editCliente(cliente){
  Materialize.updateTextFields();
  this.clienteService
        .emitirClienteEditar
        .emit(cliente);
  }
  removerCliente(index: number){
    this.dataSource
        .data
        .splice(index, 1);
    this.dataSource.data = this.dataSource.data;
    this.tooltipActions.emit({action: 'tooltip', params:['hide']});
    toast('Cliente removido com sucesso', 3000);
  }

}
