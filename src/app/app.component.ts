import {Component, EventEmitter} from '@angular/core';
import {MaterializeAction, MaterializeDirective} from "angular2-materialize";
import {ClienteService} from "./cliente.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';
  modalActions = new EventEmitter<string|MaterializeAction>();

  constructor(clienteService: ClienteService){
    clienteService.emitirClienteEditar.subscribe(cliente=>{
      this.modalActions
          .emit({action:"modal",params:['open']});
    })
  }
}
