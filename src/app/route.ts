import {RouterModule, Routes} from "@angular/router";
import {ListagemComponent} from "./listagem/listagem.component";
import {CadastroComponent} from "./cadastro/cadastro.component";
import {ModuleWithProviders} from "@angular/core";

const APP_ROUTES: Routes = [
  {path: '', component: ListagemComponent},
  {path: 'cadastro', component: CadastroComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
