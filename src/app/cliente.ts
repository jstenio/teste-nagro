export class Cliente {

  private _id: number;
  private _nome: string;
  private _email: string;
  private _dataNascimento: string;
  private _sexo: 'M'|'F';

  constructor() {
  }



  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get nome(): string {
    return this._nome;
  }

  set nome(value: string) {
    this._nome = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get dataNascimento(): string {
    return this._dataNascimento;
  }

  set dataNascimento(value: string) {
    this._dataNascimento = value;
  }

  get sexo(): 'M'|'F' {
    return this._sexo;
  }

  set sexo(value: 'M'|'F') {
    this._sexo = value;
  }

  static clienteFromData(nome: string, email: string, dataNascimento: string, sexo: 'M'|'F'): Cliente {
    let cli = new Cliente();
    cli.nome = nome;
    cli.email = email;
    cli.dataNascimento = dataNascimento;
    cli._sexo = sexo;
    return cli;
  }


}

