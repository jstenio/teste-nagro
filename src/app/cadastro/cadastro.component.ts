import {Component, EventEmitter, OnInit} from '@angular/core';
import {MaterializeDirective} from "angular2-materialize";
import {Cliente} from "../cliente";
import {ClienteService} from "../cliente.service";

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {

  cliente: Cliente;

  constructor(private clienteService: ClienteService) {
    this.cliente = new Cliente();
    this.clienteService.emitirClienteEditar.subscribe(cliente => {
      this.cliente = cliente;

    });
  }

  ngOnInit() {
  }



}
